package com.yanisallouch.inscription;

import android.content.Context;
import android.util.Log;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class File {

    public static void writeToFile(String data, Context context, String fileName) {
        try {
            OutputStreamWriter outputStreamWriter = new OutputStreamWriter(context.openFileOutput(fileName, Context.MODE_PRIVATE));
            outputStreamWriter.write(data);
            Log.e("File", "Write: " + data);
            outputStreamWriter.close();
        }
        catch (IOException e) {
            Log.e("File", "write failed: " + e.toString());
        }
    }

    public static void readFromFile(Context context, String fileName, List<String> lines ) {
        try {
            InputStream inputStream = context.openFileInput(fileName);
            if ( inputStream != null ) {
                InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
                BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
                String receiveString;
                while ((receiveString = bufferedReader.readLine()) != null) {
                    lines.add(receiveString);
                    Log.e("File", "Read: " + receiveString);
                }
                inputStream.close();
            }
        }
        catch (FileNotFoundException e) {
            Log.e("File", "Not found: " + e.toString());
        } catch (IOException e) {
            Log.e("File", "Can't read: " + e.toString());
        }
    }

    public static void writeRandomWordsToAndReadTo(Context applicationContext, String fileName, List<String> planning) {
        int leftLimit = 97; // letter 'a'
        int rightLimit = 122; // letter 'z'
        int targetStringLength = 35;
        Random random = new Random();
        String generatedString = "";
        StringBuilder buffer = new StringBuilder(targetStringLength);
        for (int j = 0; j < 4 ; j++) {
            for (int i = 0; i < targetStringLength; i++) {
                int randomLimitedInt = leftLimit + (int)
                        (random.nextFloat() * (rightLimit - leftLimit + 1));
                buffer.append((char) randomLimitedInt);
            }
            buffer.append("\n");
        }
        generatedString = buffer.toString();

        File.writeToFile(generatedString, applicationContext, fileName);
        File.readFromFile(applicationContext, fileName, planning);
    }
}