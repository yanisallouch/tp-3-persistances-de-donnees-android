package com.yanisallouch.inscription;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;

import java.util.List;

@Dao
public interface PlanningDao {
    @Query("SELECT * FROM PlanningBDD")
    List<PlanningBDD> getAll();

    @Query("SELECT * FROM PlanningBDD WHERE uid IN (:planningIds)")
    List<PlanningBDD> loadAllByIds(int[] planningIds);

    @Query("SELECT * FROM PlanningBDD WHERE periode LIKE :periode LIMIT 1")
    PlanningBDD findByPeriode(String periode);

    @Insert
    void insert(PlanningBDD planningBDD);

    @Insert
    void insertAll(PlanningBDD... planningBDDS);

    @Delete
    void delete(PlanningBDD planningBDDS);

}
