package com.yanisallouch.inscription;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity
public class PlanningBDD {
    @PrimaryKey
    public int uid;

    @ColumnInfo(name="periode")
    public String periode;

    @ColumnInfo(name = "contenu")
    public String contenu;

    public PlanningBDD(int uid, String periode, String contenu) {
        this.uid = uid;
        this.periode = periode;
        this.contenu = contenu;
    }
}
