package com.yanisallouch.inscription;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.EditText;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;

public class MainActivity2 extends AppCompatActivity {

    public static final String INTENT_FILENAME = "filename";
    Intent intent;
    FileInputStream inputStream;

    EditText editText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);

        intent = getIntent();

        String filename = intent.getStringExtra(INTENT_FILENAME);
        Log.e("MainActivity2", "filename: " + filename);

        InputStreamReader inputStreamReader = null;
        BufferedReader bufferedReader;
        String line;
        StringBuilder sb = new StringBuilder();
        try {
            inputStream = openFileInput(filename);
            inputStreamReader = new InputStreamReader(inputStream);
            bufferedReader = new BufferedReader(inputStreamReader);
            sb = new StringBuilder();

            while ((line = bufferedReader.readLine()) != null) {
                sb.append(line).append("\n");
            }
        } catch (FileNotFoundException e) {
            Log.e("MainActivity2", "Fichier: <"+ filename + "> raised: " + e.getCause());
            Log.e("MainActivity2", "File StackTrace: " + Arrays.toString(e.getStackTrace()));
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                assert inputStreamReader != null;
                inputStreamReader.close();
                inputStream.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        editText = findViewById(R.id.editTextMultiLine);
        editText.setText(sb.toString());
    }
}